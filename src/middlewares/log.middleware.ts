export default async function handleLog(ctx: any, next: any) {
  console.log(`request: [${ctx.method}]`, ctx.url);
  console.log('query:', ctx.query);
  console.log('body:', ctx.request.body);
  console.log('auth:', ctx.header.authorization);
  await next();
  console.log('response:', ctx.response.status, ctx.response.body);
}
