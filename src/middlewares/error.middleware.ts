import { error as resError } from '../utils/response';

export default async function handleErr(ctx: any, next: any) {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = resError(err);
    ctx.app.emit('error', err, ctx);
  }
}
