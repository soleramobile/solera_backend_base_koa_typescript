import jwt from 'jwt-simple';
import { error as resError } from '../utils/response';

export default async function handleValidate(ctx: any, next: any) {
  const { headers } = ctx.request;
  const { authorization } = headers;

  if (authorization) {
    try {
      const token = authorization.split(' ')[1];

      if (token) {
        ctx.state.session = jwt.decode(token, <string>process.env.JWT_KEY);
        console.debug('Sesión:', ctx.state.session);

        // let user_data = []

        // if(user_data.length == 0) {
        //   ctx.status = 401
        //   return ctx.body = await resError('', 401)
        // }

        await next();
      } else {
        ctx.body = resError('No tiene permisos para solicitar este servicio', 500, 'No tiene permisos para solicitar este servicio');
      }
    } catch (err) {
      console.error(err.message);
    }
  } else {
    ctx.body = resError('No tiene permisos para solicitar este servicio', 500, 'No tiene permisos para solicitar este servicio');
  }
}
