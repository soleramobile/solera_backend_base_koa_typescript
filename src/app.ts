import dotenv from 'dotenv';
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import cors from 'koa2-cors';
import userRouter from './modules/user/user.routes';

dotenv.config();

export default class Application {
  app: Koa;

  constructor() {
    this.app = new Koa();
    this.middleware();
  }

  middleware() {
    this.app.use(bodyParser());
    this.app.use(cors());
    this.app.use(userRouter.routes());
  }

  start() {
    this.app.listen(<string>process.env.PORT, () :void => {
      console.log(`koa started on port: ${process.env.PORT}`);
    }).on('error', (err) => console.log(err));
  }
}
