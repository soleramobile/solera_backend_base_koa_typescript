export const error = (err: any, errorCode?: number, errorDetail?: any) => {
  const isString = typeof err === 'string';
  const obj = {
    success: <boolean>false,
    error: <any>{
      errorCode: errorCode || (isString ? 990 : (err.code || 990)),
      errorMessage: isString ? err : 'Ocurrió un error', // error.message
    },
  };

  if (['development', 'testing'].indexOf(<string>process.env.NODE_ENVIROMENT) > -1) {
    obj.error.errorMessageDetail = errorDetail || (isString ? err : err.stack);
  }

  return obj;
};

export const success = (data: {} = {}, successMessage: string = 'Ok') => {
  const obj = {
    success: <boolean>true,
    body: {
      successMessage,
      data,
    },
  };
  return obj;
};
