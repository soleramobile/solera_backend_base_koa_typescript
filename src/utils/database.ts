import mongoose from 'mongoose';

export default async function connect(): Promise<void> {
  try {
    await mongoose.connect(`mongodb://localhost/${process.env.DB_NAME}`, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    console.log(`>>> Database connected: ${process.env.DB_NAME}`);
  } catch {
    console.log('Error de servidor');
  }
}
