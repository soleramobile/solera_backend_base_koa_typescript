import Router from 'koa-router';

import handleLog from '../../middlewares/log.middleware';
import handleErr from '../../middlewares/error.middleware';

import * as user from './user.controllers';

const userRouter: Router = new Router();

userRouter.post('/user/login', handleLog, handleErr, user.login);

export default userRouter;
