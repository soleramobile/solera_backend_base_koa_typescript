import Koa from 'koa';
import { success as resSuccess } from '../../utils/response';

import { IUserLogin } from './user.interfaces';

export const login = async (ctx: Koa.Context) => {
  const bodyRequest = ctx.request.body;

  const params: IUserLogin = {
    user: bodyRequest.user,
    password: bodyRequest.password,
  };

  ctx.body = await resSuccess(<IUserLogin>params, <string>'Bienvenido');
};

export const loggout = (ctx: Koa.Context) => {
  ctx.body = 'regresar';
};
