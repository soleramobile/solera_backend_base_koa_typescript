import App from './app';
import database from './utils/database';

/**
 * empezando el server
 */
database();
const app = new App();
app.start();
